﻿/**
OpenGL context & framebuffer configuration types.

Copyright: Denis Shelomovskij 2012-2014

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij

Macros:
	OPENGL_WIKI = $(HTTP www.opengl.org/wiki/$1, $2 in OpenGL wiki)
	WIKI = $(HTTP en.wikipedia.org/wiki/$1, $2)
*/
module dgl.config;


import core.stdc.string;

import std.algorithm;
import std.array;
import std.range;
import std.string;
import std.typecons;

import dgl.glapi;
import dgl.utils;


@safe pure nothrow:

/**
OpenGL API version.
*/
struct Version
{
pure:

	/// Major version part.
	ubyte major;

	/// Minor version part.
	ubyte minor;


	/**
	Constructor taking a major and a minor version parts.

	Preconditions:
	$(I major.minor) is a valid OpenGL version.
	*/
	this(in ubyte major, in ubyte minor) inout nothrow
	in
	{
		validate(major, minor);
	}
	body
	{
		this.major = major;
		this.minor = minor;
	}


	private void validate(void* = null /*dmd @@@BUG11461@@@ workaround*/) const nothrow
	{
		validate(major, minor);
	}

	private static void validate(in ubyte major, in ubyte minor, void* = null /*dmd @@@BUG11461@@@ workaround*/) nothrow
	{
		switch(major)
		{
			case 0:
				assert(0, "There is no OpenGL 0.x series. " ~
					"The first version of OpenGL, version 1.0, " ~
					"was released in January 1992 by Mark Segal and Kurt Akeley.");
			case 1:
				assert(minor <= 5, "OpenGL 1.x series ended with version 1.5 in July 2003.");
				break;
			case 2:
				assert(minor <= 1, "OpenGL 2.x series ended with version 2.1 in July 2006.");
				break;
			case 3:
				assert(minor <= 3, "OpenGL 3.x series ended with version 3.3 in March 2010.");
				break;
			default:
				// OpenGL 4.x is the current version series so assume
				// all versions starting from 4.0 are valid.
		}
	}

	/**
	Returns whether $(I major.minor) is a valid OpenGL version.

	Valid versions are $(I 1.x) up to $(I 1.5), $(I 2.0), $(I 2.1),
	$(I 3.x) up to $(I 3.3).
	For now all versions starting from $(I 4.0) are considered valid.
	*/
	static bool isValid(in ubyte major, in ubyte minor) nothrow
	{
		enum ubyte[3] knownMaxMinors = [5, 1, 3];
		return major > knownMaxMinors.length ||
			major && minor <= knownMaxMinors[major - 1];
	}

	@property const nothrow
	{
		/**
		Returns whether this object represents a _valid API version.

		See $(MREF Version.isValid).
		*/
		bool valid()
		{
			return isValid(major, minor);
		}

		/**
		Returns whether this API version has a profile.

		Versions starting from $(I 3.2) has a profile.
		*/
		bool hasProfile()
		{
			return this >= Version(3, 2);
		}

		/**
		Returns whether this API version can be forward compatible.

		Versions starting from $(I 3.0) can be forward compatible.
		*/
		bool canBeForwardCompatible()
		{
			return major >= 3;
		}
	}

	// NOTE: Default equality comparison is used so there is no need
	// to declare `opEquals` until dmd enhancement 12311 is accepted.

	/**
	Ordering comparison for two versions.
	*/
	int opCmp(in Version rhs) const nothrow
	{
		// `k` is power of 2 for faster multiplication:
		enum k = ubyte.max + 1;
		const n = major * k + minor;
		const rhsN = rhs.major * k + rhs.minor;
        return n < rhsN ? -1 : n > rhsN ? 1 : 0;
	}

	/**
	Convert version to string using specified format.
	*/
	string toString(in char[] fmt = "%d.%d") const
	{
		return format(fmt, major, minor);
	}
}

/**
OpenGL context compatibility requirements.

See $(OPENGL_WIKI Core_And_Compatibility_in_Contexts#OpenGL_3.2_and_Profiles,
Core And _Compatibility in Contexts).

Note:
This is not a flag enum so there is no way to create a forward compatible
$(I compatibility) profile. The reasons for this decision are:
$(UL
	$(LI it has no sense as $(I compatibility) profile doesn't deprecate anything;)
	$(LI some drivers fail to create forward compatible $(I compatibility) profile.)
)
*/
enum Compatibility
{
	/**
	No compatibility requirements.

	When used for versions starting from $(I 3.2) a $(I core) profile is required.

	Also this option should be used for creating $(I 1.x) and $(I 2.x) contexts
	which has _no explicit compatibility options and all are backward compatible.
	*/
	no,

	/**
	Backward compatible context.

	When used for version $(I 3.1) a $(I GL_ARB_compatibility) extension is required.
	For later versions a $(I compatibility) profile is required.

	Can be used starting from version $(I 3.1) as all previous versions are
	_backward compatible.
	*/
	backward,

	/**
	Forward compatible context.

	Any deprecated features are unavailable in such context.
	See $(OPENGL_WIKI Core_And_Compatibility_in_Contexts#Forward_compatibility,
	Forward compatibility).

	When used for versions starting from $(I 3.2) a $(I core) profile is required.

	Can be used starting from version $(I 3.0) as this version
	introduced deprecation concept.

	Warning:
	OpenGL wiki states one should hardly ever use _forward compatible context
	except for version $(I 3.0) as $(I 3.1) removed most of the stuff.
	*/
	forward,
}

/**
OpenGL context configuration description.
*/
struct ContextConfig
{
	/**
	Context version.

	See $(MREF Version).
	*/
	Version version_;

	/**
	Context _compatibility requirements.

	See $(MREF Compatibility).
	*/
	Compatibility compatibility;

	/**
	Whether context is a debug context.

	See $(OPENGL_WIKI Debug_Output, Debug Output).
	*/
	bool debug_;


	/**
	Returns whether this object represents a _valid context configuration.
	*/
	@property bool valid() const pure nothrow
	{
		return version_.valid &&
			(version_.major <= 2 ? !compatibility :
			compatibility != Compatibility.backward || version_ != Version(3, 0));
	}

	package void validate(void* = null /*dmd @@@BUG11461@@@ workaround*/) const pure nothrow
	{
		version_.validate();

		if(version_.major <= 2)
			assert(!compatibility, "OpenGL 1.x and 2.x contexts has no explicit compatibility options.");
		else if(compatibility == Compatibility.backward)
			assert(version_ != Version(3, 0), "OpenGL 3.0 context can only be forward compatible, not backward.");
	}

static:

	/**
	Returns active thread context version.
	*/
	@property Version currentVersion() @trusted
	{
		import core.stdc.string: strlen;
		import std.format: formattedRead;


		ubyte major, minor;
		ushort revision;
		const versionCString = cast(const(char)*) glGetString(GL_VERSION);
		auto versionString = versionCString[0 .. strlen(versionCString)];
		versionString.formattedRead("%s.%s.%s", &major, &minor, &revision);

		return Version(major, minor);
	}

	/**
	Returns active thread context configuration.

	Note:
	Compatibility profile is expected to not be forward compatible.
	An $(D Exception) will be throw if it is.
	*/
	@property ContextConfig current() @trusted
	{
		ContextConfig cfg =
		{
			version_: currentVersion,
			compatibility: Compatibility.no,
			debug_: false,
		};

		if(cfg.version_.canBeForwardCompatible) // starting from 3.0
		{
			GLint flags;
			glGetIntegerv(GL_CONTEXT_FLAGS, &flags);

			if(flags & GL_CONTEXT_FLAG_FORWARD_COMPATIBLE_BIT)
				cfg.compatibility = Compatibility.forward;

			if(cfg.version_.hasProfile) // starting from 3.2
			{
				GLint mask;
				glGetIntegerv(GL_CONTEXT_PROFILE_MASK, &mask);

				const backwardCompatible =
					(mask & GL_CONTEXT_COMPATIBILITY_PROFILE_BIT) ||
					!mask; // Possible because of driver bugs.

				if(backwardCompatible)
				{
					if(cfg.compatibility) // forward compatible
						throw new Exception("`ContextConfig.current` expects " ~
							"compatibility profile to not be forward compatible.");
					cfg.compatibility = Compatibility.backward;
				}
			}
			else if(cfg.version_.minor == 1) // 3.1
			{
				if(extensionSupported("GL_ARB_compatibility"))
				{
					if(cfg.compatibility) // forward compatible
						throw new Exception("`ContextConfig.current` expects " ~
							"forward compatible 3.1 context to not supported " ~
							"`GL_ARB_compatibility`.");
					cfg.compatibility = Compatibility.backward;
				}
			}
		}

		return cfg;
	}

	/**
	Returns whether active thread context support the extension.
	*/
	bool extensionSupported(in char[] nameString) @trusted nothrow
	{
		version(Windows)
			import dgl.windows.loading : tryLoad = tryLoadWGL;
		else
			static assert(0);

		const versionCString = glGetString(GL_VERSION);
		if(versionCString[0] >= '3' || versionCString[1] != '.')
		{
			GLint extensionsCount;
			glGetIntegerv(GL_NUM_EXTENSIONS, &extensionsCount);
			GetStringi glGetStringi;
			if(!tryLoad!glGetStringi())
				return false; // Just ignore it for now.
			foreach(const i; 0 .. extensionsCount)
			{
				const currNameString = glGetStringi(GL_EXTENSIONS, cast(GLuint) i);

				if(!currNameString)
					continue; // Just ignore it for now.

				if(currNameString[0 .. strlen(cast(const char*) currNameString)] == nameString)
					return true;
			}
			return false;
		}
		else
		{
			return (cast(const char*) glGetString(GL_EXTENSIONS))
				.extensionsRange().canFind(nameString);
		}
	}
}

/**
Framebuffer configuration.
*/
struct FramebufferConfig
{
	/// Color bits.
	struct ColorBits
	{
		/// Total color bits per pixel.
		ubyte total;
		/// Red, green, blue, and alpha bits per pixel.
		ubyte red;
		ubyte green;  /// ditto
		ubyte blue;   /// ditto
		ubyte alpha;  /// ditto
	}

	/// Color buffer bits.
	ColorBits colorBits = ColorBits(32, 8, 8, 8);
	/// Depth bits.
	ubyte depthBits = 24;
	/// Stencil bits.
	ubyte stencilBits = 8;

	/// Accumulation buffer bits.
	ColorBits accumBits = ColorBits(0, 0, 0, 0);

	/// Auxiliary buffers count.
	ubyte auxBuffers;

	/// Whether this framebuffer format is hardware _accelerated.
	bool accelerated = true;
	/// Whether this framebuffer format is double buffered.
	bool doubleBuffered = true;
	/**
	Whether this framebuffer format is _stereo,
	i.e. color buffer has left/right pairs.
	*/
	bool stereo = false;
	/**
	Multisample buffer coverage & color samples count.

	Non-zero values indicate multisampling is available for
	this framebuffer format. If no multisampling is available,
	both values are zero.

	$(D _coverageSamples >= _colorSamples) is always true.
	*/
	ubyte coverageSamples = 0;
	ubyte colorSamples = 0; /// ditto
	/// Whether this framebuffer format is $(WIKI SRGB, _sRGB) capable.
	bool sRGB = false;
}


/**
Structure for framebuffer configuration selection.
*/
struct FramebufferSelection
{
	/**
	Returns whether given framebuffer configuration is _usable.

	If null, every framebuffer configuration is considered _usable.
	*/
	bool delegate(in FramebufferConfig) usable;

	/**
	Every delegate in this array returns $(I weight) of a given
	framebuffer configuration.

	A configuration with the minimum $(I weight) is preffered.

	If weights contains multiple delegates, they are applied sequentially
	starting from the first one until there is the only one preffered
	configuration or there is no more delegates.

	If weights is empty or if after applying all delegates there are
	multiple preffered configurations, any one is selected.
	*/
	int delegate(in FramebufferConfig)[] weights;

	/**
	Returns whether this object represents a _valid structure for framebuffer
	configuration selection..
	*/
	@property bool valid() const pure nothrow
	{
		return weights.all!`!!a`();
	}

	private void validate(void* = null /*dmd @@@BUG11461@@@ workaround*/) const nothrow
	{
		assert(weights.all!`!!a`(), "null weight.");
	}

	package size_t choose(R)(R available) @trusted const
	if(isInputRange!R && is(ElementType!R : FramebufferConfig))
	in
	{
		assert(!available.empty);
	}
	body
	{
		auto cfgs = zip(available, iota(size_t.max))
			.filter!(pair => !usable || usable(pair[0]))().array();

		if(cfgs.empty)
			return -1;

		foreach(const weightPred; weights)
		{
			auto cfgsWithWeights = cfgs
				.map!(pair => tuple(pair, weightPred(pair[0])))().array();

			const minWeight = cfgsWithWeights
				.map!`a[1]`().minPos().front;

			cfgs = cfgsWithWeights
				.filter!(pair => pair[1] == minWeight)()
				.map!`a[0]`().array();

			if(cfgs.length == 1)
				break;
		}

		return cfgs.front[1];
	}
}

/**
Information for OpenGL context creation & framebuffer configuration selection.
*/
struct ContextCreationInfo
{
	/**
	Requested _context configuration.

	See $(MREF ContextConfig).
	*/
	ContextConfig context;

	/**
	Requested _framebuffer configuration selection.

	See $(MREF FramebufferSelection).
	*/
	FramebufferSelection framebuffer;

	/**
	Returns whether this object represents a _valid information for 
	context creation & framebuffer configuration selection.
	*/
	@property bool valid() const pure nothrow
	{
		return context.valid && framebuffer.valid;
	}

	package void validate(void* = null /*dmd @@@BUG11461@@@ workaround*/) const nothrow
	{
		context.validate();
		framebuffer.validate();
	}
}

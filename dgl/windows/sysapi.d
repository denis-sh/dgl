﻿/**
WinAPI declarations.

Copyright: Denis Shelomovskij 2014

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module dgl.windows.sysapi;


import core.sys.windows.windows;


extern(Windows) nothrow extern:

enum : int
{
	GWL_STYLE = -16,
	GWL_USERDATA = -21,
	GCL_STYLE = -26,
}

void SetLastError(DWORD dwErrCode);

BOOL SetWindowTextW(HWND hWnd, LPCWSTR lpString);

DWORD GetWindowLongW(HWND hWnd, int nIndex);
DWORD SetWindowLongW(HWND hWnd, int nIndex, LONG dwNewLong);

version (Win64)
{
	LONG_PTR GetWindowLongPtrW(HWND hWnd, int nIndex);
	LONG_PTR SetWindowLongPtrW(HWND hWnd, int nIndex, LONG_PTR dwNewLong);
}
else
{
	alias GetWindowLongPtrW = GetWindowLongW;
	alias SetWindowLongPtrW = SetWindowLongW;
}


enum
{
	SWP_NOSIZE          = 0x0001,
	SWP_NOMOVE          = 0x0002,
	SWP_NOZORDER        = 0x0004,
	SWP_NOREDRAW        = 0x0008,
	SWP_NOACTIVATE      = 0x0010,
	SWP_FRAMECHANGED    = 0x0020,
	SWP_SHOWWINDOW      = 0x0040,
	SWP_HIDEWINDOW      = 0x0080,
	SWP_NOCOPYBITS      = 0x0100,
	SWP_NOOWNERZORDER   = 0x0200,
	SWP_NOSENDCHANGING  = 0x0400,

	SPI_GETWORKAREA = 0x0030,

	WM_WINDOWPOSCHANGED = 0x0047,
}

struct WINDOWPOS
{
	HWND    hwnd;
	HWND    hwndInsertAfter;
	int     x;
	int     y;
	int     cx;
	int     cy;
	UINT    flags;
}

BOOL SetWindowPos(
	HWND hWnd,
	HWND hWndInsertAfter,
	int X, int Y,
	int cx, int cy,
	UINT uFlags);

BOOL SystemParametersInfoW(UINT uiAction, UINT uiParam, PVOID pvParam, UINT fWinIni);

DWORD GetClassLongW(HWND hWnd, int nIndex);
BOOL DestroyWindow(HWND hWnd);
int  GetPixelFormat(HDC hdc);

BOOL UnregisterClassW(LPCWSTR lpClassName, HINSTANCE hInstance);
int  DescribePixelFormat(HDC hdc, int iPixelFormat, UINT nBytes, PIXELFORMATDESCRIPTOR* ppfd);


// WGL declarations:

alias PROC = INT_PTR function();


BOOL  wglCopyContext(HGLRC, HGLRC, UINT);
HGLRC wglCreateContext(HDC);
HGLRC wglCreateLayerContext(HDC, int);
BOOL  wglDeleteContext(HGLRC);
HGLRC wglGetCurrentContext();
HDC   wglGetCurrentDC();
PROC  wglGetProcAddress(LPCSTR);
BOOL  wglMakeCurrent(HDC, HGLRC);
BOOL  wglShareLists(HGLRC, HGLRC);

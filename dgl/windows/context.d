﻿/**
OpenGL rendering _context on Windows.

Copyright: Denis Shelomovskij 2014

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module dgl.windows.context;


import core.sys.windows.windows;

import std.algorithm;
import std.array;
import std.exception: enforce;

import dgl.glapi;
import dgl.config;
import dgl.windows.sysapi;
import dgl.windows.exception;
import dgl.windows.loading;
import dgl.windows.framebuffer;
import dgl.windows.contextcreation;
import dgl.windows.swapinterval;


/**
Represents OpenGL rendering context on Windows.

Warning:
Its contruction relies on an assumption the same ICD driver
is used for sequential rendering context creation on different
accelerated pixel formats.
*/
final class WGLContext
{
	private
	{
		HGLRC _handle;
		ContextConfig _config;
		FramebufferConfig _framebufferConfig;
		const int _pixelFormat;
		SwapIntervalExtensions _siExt;
		DummyWindow _window = DummyWindow.init;
	}


	/**
	Creates a new rendering context with desired context & framebuffer
	configuration using internal dummy window.

	Note:
	Context created with this constructor should be disposed in
	its creation thread.

	Preconditions:
	$(D creationInfo) is valid.
	*/
	this(in ContextCreationInfo creationInfo)
	{
		_window = DummyWindow(0);
		this(creationInfo, _window.dcHandle);
	}

	/**
	Creates a new rendering context with desired context & framebuffer
	configuration using $(D dcHandle).

	Preconditions:
	$(D creationInfo) is valid.
	$(D dcHandle) is a handle to a device context suitable for rendering context creation.
	Device context pixel format isn't set.
	*/
	this(in ContextCreationInfo creationInfo, HDC dcHandle)
	in
	{
		creationInfo.validate();
		assert(dcHandle, "null device context handle.");
		assert(!GetPixelFormat(dcHandle),
			"The device context should not have set pixel format.");
		SetLastError(0);
	}
	body
	{
		auto ccExt = CreateContextExtensions(true);

		const fbCfgs = getFramebufferConfigs(dcHandle, true, ccExt.fb).array();
		const size_t fbCfgIdx = creationInfo.framebuffer.choose(fbCfgs.map!`a[0]`());
		enforce(fbCfgIdx != -1, "Can't find any suitable framebuffer config.");
		const fbCfg = fbCfgs[fbCfgIdx];

		if(!fbCfg[0].accelerated)
			ccExt = CreateContextExtensions(false);

		this(creationInfo.context, dcHandle, fbCfg[1], true, ccExt);
	}

	/**
	Creates a new rendering context with desired configuration using $(D dcHandle)
	with choosen pixel format.

	Preconditions:
	$(D config) is valid.
	$(D dcHandle) is a handle to a device context suitable for rendering context creation.
	Device context pixel format is set to a pixel format with OpenGL support.
	*/
	this(in ContextConfig config, HDC dcHandle)
	in
	{
		config.validate();
		assert(dcHandle, "null device context handle.");
		const pixelFormat = GetPixelFormat(dcHandle);
		assert(pixelFormat, "The device context should have set pixel format.");
		PIXELFORMATDESCRIPTOR pfd = void;
		assert(DescribePixelFormat(dcHandle, pixelFormat, pfd.sizeof, &pfd),
			"Failed to describe device context's pixel format.");
		assert(pfd.dwFlags & PFD_SUPPORT_OPENGL,
			"Device context's pixel format should support OpenGL.");
	}
	body
	{
		const pixelFormat = enforceSys!GetPixelFormat(dcHandle);

		PIXELFORMATDESCRIPTOR pfd = void;
		enforceSys!DescribePixelFormat(dcHandle, pixelFormat, pfd.sizeof, &pfd);
		const accelerated = !(pfd.dwFlags & PFD_GENERIC_FORMAT);
		auto ccExt = CreateContextExtensions(accelerated);

		this(config, dcHandle, pixelFormat, false, ccExt);
	}

	private this(in ContextConfig config, HDC dcHandle, in int pixelFormat,
		in bool setPixelFormat, in CreateContextExtensions ccExt)
	in
	{
		assert(GetPixelFormat(dcHandle) == (setPixelFormat ? 0 : pixelFormat));
		SetLastError(0);
	}
	body
	{
		_pixelFormat = pixelFormat;

		if(setPixelFormat)
			setThisPixelFormat(dcHandle);

		_handle = createContext(dcHandle, null, config, ccExt);
		_config = ContextConfig.current;
		_config.debug_ = config.debug_;

		if(!getPixelFormat(dcHandle, true, pixelFormat, ccExt.fb, _framebufferConfig))
			throw new SysException("Unsupported pixel format.");

		const extensions = getWGLExtensions(dcHandle);
		_siExt = SwapIntervalExtensions(str => !!(str in extensions));
	}


	@property pure nothrow
	{
		/**
		Gets whether the rendering context is _disposed.
		*/
		bool disposed() const
		{
			return !_handle;
		}

		/**
		Gets the rendering context _handle.
		*/
		inout(HGLRC) handle() inout
		{
			return _handle;
		}

		/**
		Gets the rendering context configuration.

		See $(DPREF _config, ContextConfig).
		*/
		ContextConfig config() const
		{
			return _config;
		}

		/**
		Gets the rendering context framebuffer configuration.

		See $(DPREF config, FramebufferConfig).
		*/
		FramebufferConfig framebufferConfig() const
		{
			return _framebufferConfig;
		}

		/**
		Gets the rendering context pixel format index.

		The rendering context is suitable for using with any device context
		sharing this pixel format and created on the same device.
		*/
		int pixelFormat() const
		{
			return _pixelFormat;
		}
	}

	/**
	Sets device context's pixel format to this rendering context pixel format.
	*/
	void setThisPixelFormat(HDC dcHandle) const
	in { assert(pixelFormat); }
	body
	{
		PIXELFORMATDESCRIPTOR pfd = void;
		enforceSys!DescribePixelFormat(dcHandle, pixelFormat, pfd.sizeof, &pfd);
		enforceSys!SetPixelFormat(dcHandle, pixelFormat, &pfd);
	}

	/**
	Makes the rendering context the calling thread's current rendering context
	using $(D dcHandle).

	Preconditions:
	$(D dcHandle) is a handle to a device context sharing this rendering
	context's pixel format.
	*/
	void makeCurrent(HDC dcHandle)
	in
	{
		const pf = GetPixelFormat(dcHandle);
		SetLastError(0);
		assert(pf, "The device context should have set pixel format.");
		assert(pf == pixelFormat,
			"The device context should share this rendering context's pixel format.");
	}
	body
	{
		// Logically no-op rendering context switch may be expensive too:
		if(isCurrent && wglGetCurrentDC() == dcHandle)
			return;

		enforceSys!wglMakeCurrent(dcHandle, handle);
	}

	/**
	Returns wheter this rendering context is the calling thread's
	current rendering context.
	*/
	@property bool isCurrent() const
	{
		return handle == currentHandle;
	}

	/**
	Gets the handle to the calling thread's current rendering context.
	*/
	static @property HGLRC currentHandle()
	{
		return wglGetCurrentContext();
	}

	/**
	Makes the calling thread's current rendering context no longer current and releases
	the device context that is used by the rendering context.
	*/
	static void releaseCurrent()
	{
		enforceSys!wglMakeCurrent(null, null);
	}

	/**
	Swaps device context color buffers.

	If $(D wait) is true also waits using $(D glFinish) for
	actual swap to take effect.
	
	Note:
	Setting $(D wait) to false may cause flickering on
	control resizing.
	*/
	static void swapBuffers(HDC dcHandle, in bool wait = true)
	{
		enforceSys!SwapBuffers(dcHandle);
		if(wait)
			glFinish();
	}

	/**
	Returns wheter the rendering context support setting a swap interval.
	
	Swap interval is a minimum number of video frame periods per buffer swap.

	See $(HTTP www.opengl.org/registry/specs/EXT/wgl_swap_control.txt,
	EXT_swap_control) extension.
	*/
	@property bool controlableSwapInterval() const
	{
		return _siExt.has_EXT_swap_control;
	}

	/**
	Returns wheter the rendering context support setting a negative
	swap interval meaning late swaps occur without synchronization
	to the video frame.

	See $(HTTP www.opengl.org/registry/specs/EXT/wgl_swap_control_tear.txt,
	EXT_swap_control_tear) extension.
	*/
	@property bool hasAdaptiveSwapInterval() const
	{
		return _siExt.has_EXT_swap_control_tear;
	}

	/**
	Gets or sets current device context's swap interval.

	Preconditions:
	$(D isCurrent && controlableSwapInterval).
	If $(D interval) is negative $(D hasAdaptiveSwapInterval) is also required.
	*/
	@property void dcSwapInterval(in int interval)
	in
	{
		assert(isCurrent);
		assert(controlableSwapInterval);
		if(!hasAdaptiveSwapInterval)
			assert(interval >= 0, "Adaptive window swap interval is not supported by " ~
				"OpenGL implementation as `EXT_swap_control_tear` isn't supported.");
	}
	body
	{
		enforceSys(_siExt.swapIntervalEXT(interval), "wglSwapIntervalEXT");
	}

	/// ditto
	@property int dcSwapInterval() const
	in
	{
		assert(isCurrent);
		assert(controlableSwapInterval);
	}
	body
	{
		return _siExt.getSwapIntervalEXT();
	}


	/**
	Releases all resources used by this class instance.
	
	Deletes the rendering context preliminarily releasing it if
	it's the calling thread's current rendering context.
	Also deletes the internal dummy window if it was created.

	This function will be called by destructor if not already disposed.

	Preconditions:
	$(D !disposed)
	*/
	void dispose()
	in { assert(!disposed); }
	out { assert(disposed); }
	body
	{
		if(isCurrent)
			releaseCurrent();
		enforceSys!wglDeleteContext(handle);
		if(_window.handle)
		{
			if(_window.creationThreadId != GetCurrentThreadId())
				assert(0, "'WGLContext' created without window can't be disposed in different thread.");
			_window.close();
		}
		_handle = null;
	}

	~this()
	{
 		if(!disposed)
			dispose();
	}
}

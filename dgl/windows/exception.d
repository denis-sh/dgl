﻿/**
WinAPI and WGL error handling module.

Copyright: Denis Shelomovskij 2014

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module dgl.windows.exception;


import core.sys.windows.windows;

import std.exception;
import std.string;
import std.traits: functionLinkage, ParameterTypeTuple;
import std.windows.syserror;


/**
Exception thrown on system (WinAPI or WGL) function failure.
*/
class SysException: Exception
{
	this(in string message, in string file = __FILE__, in size_t line = __LINE__, Throwable next = null) @safe pure nothrow
	{ super(message, file, line, next); }

	this(in string functionName, in DWORD lastError, in string[WORD] newErrors = null,
		in string file = __FILE__, in size_t line = __LINE__, Throwable next = null) @trusted nothrow
	{
		// E.g. NVIDIA drivers may storce 0xC007 in error high word.
		const lastErrorLowWord = cast(WORD) lastError;
		string lastErrorString;
		if(const strPtr = lastErrorLowWord in newErrors)
			lastErrorString = ' ' ~ *strPtr;
		else
			lastErrorString = (' ' ~ lastErrorLowWord.sysErrorString())
				.ifThrown(null).assumeWontThrow();
		if(lastErrorString && lastError != lastErrorLowWord)
			lastErrorString = " Low word error:" ~ lastErrorString;

		const msg = format("System function '%s' failed. %s",
			functionName,
			lastError ?
				format("Last error code: 0x%X.%s",
					lastError, lastErrorString) :
				"No last error."
			).assumeWontThrow();
		super(msg, file, line, next);
	}
}

/**
Convinient template to call system (WinAPI or WGL) function and throw $(MREF SysException)
on failure.
*/
template enforceSys(alias func)
if(functionLinkage!func == "Windows")
{
	auto enforceSys(ParameterTypeTuple!func args, in string[WORD] newErrors = null,
		in string file = __FILE__, in size_t line = __LINE__)
	{
		return enforceSys(func(args), __traits(identifier, func), newErrors, file, line);
	}
}

/// ditto
T enforceSys(T)(lazy T expr, lazy string funcName, in string[WORD] newErrors = null,
	in string file = __FILE__, in size_t line = __LINE__)
{
	if(T res = expr)
		return res;
	const lastError = GetLastError();
	throw new SysException(funcName, lastError, newErrors, file, line);
}

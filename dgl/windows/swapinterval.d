﻿/**
WGL functions for swap interval control.

Copyright: Denis Shelomovskij 2014

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module dgl.windows.swapinterval;


import core.sys.windows.windows;

import dgl.windows.loading;


struct SwapIntervalExtensions
{
	bool has_EXT_swap_control = false;
	bool has_EXT_swap_control_tear = false;

	extern(Windows) nothrow
	{
		// WGL_EXT_swap_control
		BOOL function(int) swapIntervalEXT = null;
		int function() getSwapIntervalEXT = null;
	}

	@disable this();

	this(scope bool delegate(in char[]) nothrow extensionSupported)
	{
		if(extensionSupported("WGL_EXT_swap_control"))
		{
			has_EXT_swap_control = true;
			loadWGL!swapIntervalEXT("wglSwapIntervalEXT");
			loadWGL!getSwapIntervalEXT("wglGetSwapIntervalEXT");

			if(extensionSupported("WGL_EXT_swap_control_tear"))
				has_EXT_swap_control_tear = true;
		}
	}
}

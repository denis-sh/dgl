﻿/**
Framebuffer related WGL functions.

Copyright: Denis Shelomovskij 2014

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module dgl.windows.framebuffer;


import core.sys.windows.windows;

import std.typecons: Tuple;

import dgl.config;
import dgl.windows.sysapi;
import dgl.windows.exception;
import dgl.windows.loading;


struct FramebufferExtensions
{
	bool has_ARB_pixel_format = false;
	bool has_multisample = false;
	bool has_NV_multisample_coverage = false;
	bool has_framebuffer_sRGB = false;

	extern(Windows) nothrow
	{
		// {WGL_ARB,EXT}_pixel_format
		/*
		wglGetPixelFormatAttribiv{ARB,EXT} signatures are almost equal
		except *_ARB one accept attributes by `const`. Use weaker here.
		*/
		BOOL function(HDC, int, int, UINT, int*, int*)
			getPixelFormatAttribiv = null;
	}

	@disable this();

	this(scope bool delegate(in char[]) nothrow extensionSupported) nothrow
	{
		if(extensionSupported("WGL_ARB_pixel_format"))
			has_ARB_pixel_format = tryLoadWGL!getPixelFormatAttribiv("wglGetPixelFormatAttribivARB");
		if(!has_ARB_pixel_format && extensionSupported("WGL_EXT_pixel_format"))
			tryLoadWGL!getPixelFormatAttribiv("wglGetPixelFormatAttribivEXT");

		if(extensionSupported("WGL_ARB_multisample") ||
			extensionSupported("WGL_EXT_multisample"))
		{
			has_multisample = true;
			if(extensionSupported("WGL_NV_multisample_coverage"))
				has_NV_multisample_coverage = true;
		}

		if(extensionSupported("WGL_ARB_framebuffer_sRGB") ||
			extensionSupported("WGL_EXT_framebuffer_sRGB"))
			has_framebuffer_sRGB = true;
	}

	private @property string getPixelFormatFuncName() const @safe pure nothrow
	{
		return "wglGetPixelFormatAttribiv" ~ (has_ARB_pixel_format ? "ARB" : "EXT");
	}
}

/**
Return a forward range of available and usable framebuffer configs.

If $(I drawToWindow) is true, the configs should be usable with a window.
*/
auto getFramebufferConfigs(HDC dcHandle, in bool drawToWindow, in FramebufferExtensions fbExt)
{
	static struct Result
	{
		private
		{
			HDC dcHandle;
			const bool drawToWindow;
			const FramebufferExtensions fbExt;
			size_t i;
			const size_t count;
			FramebufferConfig _front;
		}

		@property pure nothrow
		{
			auto save() inout
			{ return this; }

			bool empty() const
			{ return i > count; }

			auto front() const
			in { assert(!empty, "Trying to call `front` on an empty framebuffer configs range."); }
			body { return Tuple!(FramebufferConfig, int)(_front, cast(int) i); }
		}

		void popFront()
		{
			assert(!empty);
			for(++i; !empty; ++i)
			{
				if(!getPixelFormat(dcHandle, drawToWindow, i, fbExt, _front))
					continue;
				break;
			}
		}
	}

	size_t count;
	if(!drawToWindow && fbExt.getPixelFormatAttribiv)
	{
		int attribute = 0x2000 /* WGL_NUMBER_PIXEL_FORMATS_{ARB,EXT} */;
		int value;
		const getFuncName = fbExt.getPixelFormatFuncName;
		enforceSys(fbExt.getPixelFormatAttribiv(dcHandle, 1, 0, 1, &attribute, &value), getFuncName);
		if(value <= 0)
			throw new SysException("'" ~ getFuncName ~ "' returned unexpected value.");
		count = cast(size_t) value;
	}
	else
	{
		// `DescribePixelFormat` returns displayable formats count.
		count = cast(size_t) enforceSys!DescribePixelFormat(dcHandle, 1, PIXELFORMATDESCRIPTOR.sizeof, null);
	}

	auto res = Result(dcHandle, drawToWindow, fbExt, 0, count);
	res.popFront();
	if(res.empty)
		throw new Exception("Failed to find any usable FramebufferConfig-s");
	return res;
}
/*
Returns whether $(I dcHandle)'s $(I index)-th pixel format is usable.

To be usable, a pixel format should:
$(UL
	$(LI support OpenGL;)
	$(LI be RGBA, i.e. not color-indexed;)
	$(LI support drawing to window if $(I drawToWindow) is true.)
)
*/
bool getPixelFormat(HDC dcHandle, in bool drawToWindow, in int index,
	in FramebufferExtensions fbExt, out FramebufferConfig config)
{
	enum Acceleration : bool
	{
		software = false, // MS software implementation
		mcd      = false, // Mini Client Driver (outdated)
		icd      = true , // Installable Client Driver
	}

	if(fbExt.getPixelFormatAttribiv)
	{
		/*
		Extensions used:
		* {WGL_ARB,EXT}_pixel_format
		* {ARB,EXT}_multisample
		* {ARB,EXT}_framebuffer_sRGB

		*_ARB token values are equal to *_EXT ones except WGL_TRANSPARENT_VALUE_EXT
		is replaced by 5x WGL_TRANSPARENT_*_VALUE_ARB tokens.
		*/

		/* Ignored:
		Palette-managed device: WGL_NEED{,_SYSTEM}_PALETTE_{ARB,EXT}
		Independent layer planes swap: WGL_SWAP_LAYER_BUFFERS_{ARB,EXT}
		Swap strategy: WGL_SWAP_METHOD_{ARB,EXT}
		Plane numbers: WGL_NUMBER_{OVERLAYS,UNDERLAYS}_{ARB,EXT}
		Transparency: WGL_TRANSPARENT_{ARB,EXT,VALUE_EXT}, 5x WGL_TRANSPARENT_*_VALUE_ARB
		Layer plane sharing: WGL_SHARE_{DEPTH,STENCIL,ACCUM}_{ARB,EXT}
		GDI support: WGL_SUPPORT_GDI_{ARB,EXT}
		Color shifts: WGL_{RED,GREEN,BLUE,ALPHA}_SHIFT_{ARB,EXT}
		*/

		enum size_t minN = 19, maxN = 22;
		int[maxN] attributes =
		[
			/*  0 */ 0x2001 /* WGL_DRAW_TO_WINDOW_{ARB,EXT} */, // plane independent
			/*  1 */ 0x2010 /* WGL_SUPPORT_OPENGL_{ARB,EXT} */,
			/*  2 */ 0x2013 /* WGL_PIXEL_TYPE_{ARB,EXT} */,

			/*  3 */ 0x2014 /* WGL_COLOR_BITS_{ARB,EXT} */,
			/*  4 */ 0x2015 /* WGL_RED_BITS_{ARB,EXT} */,
			/*  5 */ 0x2017 /* WGL_GREEN_BITS_{ARB,EXT} */,
			/*  6 */ 0x2019 /* WGL_BLUE_BITS_{ARB,EXT} */,
			/*  7 */ 0x201B /* WGL_ALPHA_BITS_{ARB,EXT} */,

			/*  8 */ 0x2022 /* WGL_DEPTH_BITS_{ARB,EXT} */,
			/*  9 */ 0x2023 /* WGL_STENCIL_BITS_{ARB,EXT} */,

			/* 10 */ 0x201D /* WGL_ACCUM_BITS_{ARB,EXT} */,
			/* 11 */ 0x201E /* WGL_ACCUM_RED_BITS_{ARB,EXT} */,
			/* 12 */ 0x201F /* WGL_ACCUM_GREEN_BITS_{ARB,EXT} */,
			/* 13 */ 0x2020 /* WGL_ACCUM_BLUE_BITS_{ARB,EXT} */,
			/* 14 */ 0x2021 /* WGL_ACCUM_ALPHA_BITS_{ARB,EXT} */,

			/* 15 */ 0x2024 /* WGL_AUX_BUFFERS_{ARB,EXT} */,

			/* 16 */ 0x2003 /* WGL_ACCELERATION_{ARB,EXT} */,
			/* 17 */ 0x2011 /* WGL_DOUBLE_BUFFER_{ARB,EXT} */,
			/* 18 */ 0x2012 /* WGL_STEREO_{ARB,EXT} */,

			/* 19-21 */ 0, 0, 0,
		];
		int[maxN] values;

		size_t n = minN;

		/*
		Assume used wglGetPixelFormatAttribiv{ARB,EXT}
		works with used {ARB,EXT}_multisample and
		{ARB,EXT}_framebuffer_sRGB extension.
		*/
		if(fbExt.has_multisample)
		{
			attributes[n++] = 0x2042 /* WGL_SAMPLES_{ARB,EXT}, WGL_COVERAGE_SAMPLES_NV */;
			if(fbExt.has_NV_multisample_coverage)
				attributes[n++] = 0x20B9 /* WGL_COLOR_SAMPLES_NV */;
		}
		if(fbExt.has_framebuffer_sRGB)
			attributes[n++] = 0x20A9 /* WGL_FRAMEBUFFER_SRGB_CAPABLE_{ARB,EXT} */;

		const getFuncName = fbExt.getPixelFormatFuncName;

		enforceSys(fbExt.getPixelFormatAttribiv(dcHandle, index, 0, cast(int) n, attributes.ptr, values.ptr), getFuncName);

		bool isSet(in int i) @safe nothrow
		{ return !!values[i]; }

		ubyte getUbyte(in int i) @safe
		{
			const int val = values[i];
			if(val < 0 || val > ubyte.max)
				throw new SysException("'" ~ getFuncName ~ "' returned unexpected value.");
			return cast(ubyte) val;
		}

		if(drawToWindow && !values[0] ||
			!values[1] || !values[2])
			return false;

		const FramebufferConfig.ColorBits colorBits =
		{
			total  : getUbyte(3),
			red    : getUbyte(4),
			green  : getUbyte(5),
			blue   : getUbyte(6),
			alpha  : getUbyte(7),
		};

		const FramebufferConfig.ColorBits accumBits =
		{
			total  : getUbyte(10),
			red    : getUbyte(11),
			green  : getUbyte(12),
			blue   : getUbyte(13),
			alpha  : getUbyte(14),
		};

		FramebufferConfig cfg =
		{
			colorBits   : colorBits,
			depthBits   : getUbyte(8),
			stencilBits : getUbyte(9),
			accumBits   : accumBits,
			auxBuffers  : getUbyte(15),

			accelerated : values[16] == 0x2027 /* WGL_FULL_ACCELERATION_{ARB,EXT} */ ? Acceleration.icd :
				values[16] == 0x2026 /* WGL_GENERIC_ACCELERATION_{ARB,EXT} */ ? Acceleration.mcd : Acceleration.software,
			doubleBuffered : !!values[17],
			stereo : !!values[18],
		};

		n = minN;

		cfg.coverageSamples = fbExt.has_multisample ? getUbyte(n++) : 0;
		cfg.colorSamples = fbExt.has_NV_multisample_coverage ? getUbyte(n++) : cfg.coverageSamples;
		cfg.sRGB = fbExt.has_framebuffer_sRGB ? !!values[n++] : 0;

		config = cfg;
	}
	else
	{
		/* Ignored:
		Palette-managed device: PFD_NEED{,_SYSTEM}_PALETTE
		Independent layer planes swap: dwFlags & PFD_SWAP_LAYER_BUFFERS
		Plane numbers: bReserved
		Transparency: dwVisibleMask
		GDI support: PFD_SUPPORT_GDI
		Total color bit counts: c{Color,Accum}Bits
		Color shifts: c{Red,Green,Blue,Alpha}Shift
		Obsolete: iLayerType, dwLayerMask, dwDamageMask
		*/

		PIXELFORMATDESCRIPTOR pfd = void;
		enforceSys!DescribePixelFormat(dcHandle, index, PIXELFORMATDESCRIPTOR.sizeof, &pfd);

		bool hasFlag(DWORD flag) @safe nothrow
		{ return !!(pfd.dwFlags & flag); }

		if(drawToWindow && !hasFlag(PFD_DRAW_TO_WINDOW) ||
			!hasFlag(PFD_SUPPORT_OPENGL) ||
			pfd.iPixelType != PFD_TYPE_RGBA)
			return false;

		const FramebufferConfig.ColorBits colorBits =
		{
			total  : pfd.cColorBits,
			red    : pfd.cRedBits,
			green  : pfd.cGreenBits,
			blue   : pfd.cBlueBits,
			alpha  : pfd.cAlphaBits,
		};

		const FramebufferConfig.ColorBits accumBits =
		{
			total  : pfd.cAccumBits,
			red    : pfd.cAccumRedBits,
			green  : pfd.cAccumGreenBits,
			blue   : pfd.cAccumBlueBits,
			alpha  : pfd.cAccumAlphaBits,
		};

		const FramebufferConfig cfg =
		{
			colorBits      : colorBits,
			depthBits      : pfd.cDepthBits,
			stencilBits    : pfd.cStencilBits,
			accumBits      : accumBits,
			auxBuffers     : pfd.cAuxBuffers,

			accelerated : hasFlag(PFD_GENERIC_ACCELERATED) ? Acceleration.mcd :
				hasFlag(PFD_GENERIC_FORMAT) ? Acceleration.software : Acceleration.icd,
			doubleBuffered : hasFlag(PFD_DOUBLEBUFFER),
			stereo : hasFlag(PFD_STEREO),

			coverageSamples : 0,
			colorSamples : 0,
			sRGB : false,
		};
		config = cfg;
	}

	return true;
}

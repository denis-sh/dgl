﻿/**
WGL functions for context creation.

Copyright: Denis Shelomovskij 2014

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module dgl.windows.contextcreation;


import core.atomic;
import core.sync.mutex;
import core.sys.windows.windows;

import std.exception: enforce;
import std.string: format;
import std.traits: fullyQualifiedName;
import std.utf: toUTF16z;

import dgl.config;
import dgl.windows.sysapi;
import dgl.windows.exception;
import dgl.windows.loading;
import dgl.windows.framebuffer;


struct WindowClass
{
	private
	{
		WNDCLASSEXW wndClass;
		HINSTANCE processInstance = null;
		Mutex mutex;
		shared size_t windows = 0;
		shared ATOM classAtom = 0;
	}

	@disable this();
	@disable this(this);

	this(WNDCLASSEXW wndClass)
	{
		this.wndClass = wndClass;
		processInstance = wndClass.hInstance;
		mutex = new Mutex();
	}

	HWND createWindow(
		in DWORD exStyle,
		in char[] name,
		in DWORD style,
		in int x, in int y,
		in int width, in int height,
		HWND parentWindow,
		HMENU menu,
		LPVOID param)
	{
		windows.atomicOp!`+=`(cast(size_t) 1);
		scope(failure) onWindowDestroyed();

		synchronized(mutex)
		{
			if(!classAtom)
				classAtom = enforceSys!RegisterClassExW(&wndClass);
		}

		HWND handle = enforceSys!CreateWindowExW
		(
			exStyle,
			cast(LPCWSTR) classAtom,
			name.toUTF16z(),
			style,
			x, y,
			width, height,
			parentWindow,
			menu,
			processInstance,
			param,
		);

		return handle;
	}

	void destroyWindow(HWND handle)
	{
		assert(windows.atomicLoad());
		enforceSys!DestroyWindow(handle);
		onWindowDestroyed();
	}

	private void onWindowDestroyed()
	{
		if(windows.atomicOp!`-=`(cast(size_t) 1))
			return;

		synchronized(mutex)
		{
			if(windows.atomicLoad() || !classAtom)
				return;
			enforceSys!UnregisterClassW(cast(LPCWSTR) classAtom, processInstance);
			classAtom = 0;
		}
	}
}


struct DummyWindow
{
	private
	{
		__gshared WindowClass windowClass = WindowClass.init;
		HWND _handle;
		HDC _dcHandle;
		DWORD _creationThreadId;
	}

	@trusted shared static this()
	{
		WNDCLASSEXW wndClass =
		{
			cbSize: WNDCLASSEXW.sizeof,
			style: CS_OWNDC,
			lpfnWndProc: &DefWindowProcW,
			hInstance: enforceSys!GetModuleHandleW(null),
			lpszClassName: (fullyQualifiedName!(typeof(this)) ~ "\0"w).ptr,
		};

		windowClass = WindowClass(wndClass);
	}

	@disable this();
	@disable this(this);

	this(in int)
	{
		_handle = windowClass.createWindow
		(
			0, // ex style
			null, 0, // window name, style
			0, 0, 1, 1, // x, y, width, height
			null, null, // parent window, menu
			null, // param
		);

		scope(failure) windowClass.destroyWindow(handle);

		// NOTE: No need to release private DC.
		_dcHandle = enforceSys!GetDC(handle);

		_creationThreadId = GetCurrentThreadId();
	}

	void close()
	in { assert(handle && creationThreadId == GetCurrentThreadId()); }
	body
	{
		windowClass.destroyWindow(handle);
		_handle = null;
	}

	~this()
	{
		if(handle)
		{
			if(creationThreadId != GetCurrentThreadId())
				assert(0, "Opened 'DummyWindow' can't be destroyed in different thread.");
			close();
		}
	}

	@property pure nothrow
	{
		DWORD creationThreadId() const
		{ return _creationThreadId; }

		inout(HWND) handle() inout
		{ return _handle; }

		inout(HDC) dcHandle() inout
		{ return _dcHandle; }
	}
}


struct TempContext
{
	private
	{
		HINSTANCE _processInstance;
		ATOM _classAtom;
		HWND _handle;
		HDC _dcHandle;
		bool _accelerated;
		HGLRC _glrc;
	}

	@disable this();
	@disable this(this);

	this(in bool useAccelerationIfAny)
	{
		_processInstance = enforceSys!GetModuleHandleW(null);

		const className = format("%s-%s", fullyQualifiedName!(typeof(this)),
			GetCurrentThreadId());

		const WNDCLASSEXW wndClass =
		{
			cbSize: WNDCLASSEXW.sizeof,
			style: CS_OWNDC,
			lpfnWndProc: &DefWindowProcW,
			hInstance: _processInstance,
			lpszClassName: className.toUTF16z(),
		};

		const classAtom = _classAtom = enforceSys!RegisterClassExW(&wndClass);

		scope(failure) enforceSys!
			UnregisterClassW(cast(LPCWSTR) classAtom, _processInstance);


		HWND handle = _handle = enforceSys!CreateWindowExW
		(
			0, // ex style
			cast(LPCWSTR) classAtom, // class name or atom
			null, 0, // window name, style
			0, 0, 1, 1, // x, y, width, height
			null, null, // parent window, menu
			_processInstance, // instance
			null, // lpParam
		);

		scope(failure) enforceSys!DestroyWindow(handle);

		// NOTE: No need to release private DC.
		HDC dcHandle = _dcHandle = enforceSys!GetDC(handle);


		const maxDisplayableFormatIndex = cast(size_t) enforceSys!
			DescribePixelFormat(dcHandle, 1, PIXELFORMATDESCRIPTOR.sizeof, null);

		static bool usablePixelFormat(in PIXELFORMATDESCRIPTOR pfd, in bool accelerated) @safe pure nothrow
		{
			return pfd.dwFlags & PFD_DRAW_TO_WINDOW && pfd.dwFlags & PFD_SUPPORT_OPENGL &&
				!(pfd.dwFlags & PFD_GENERIC_ACCELERATED) && // Not an MCD format.
				!(pfd.dwFlags & PFD_GENERIC_FORMAT) == accelerated;
		}

		const accelerated = _accelerated =
		{
			if(useAccelerationIfAny)
			{
				foreach(const pixelFormatIndex; 1 .. maxDisplayableFormatIndex + 1)
				{
					PIXELFORMATDESCRIPTOR pfd = void;
					enforceSys!DescribePixelFormat(dcHandle, pixelFormatIndex, PIXELFORMATDESCRIPTOR.sizeof, &pfd);
					if(usablePixelFormat(pfd, true))
						return true;
				}
			}
			return false;
		} ();

		foreach(const pixelFormatIndex; 1 .. maxDisplayableFormatIndex + 1)
		{
			PIXELFORMATDESCRIPTOR pfd = void;
			enforceSys!DescribePixelFormat(dcHandle, pixelFormatIndex, PIXELFORMATDESCRIPTOR.sizeof, &pfd);

			bool hasFlag(DWORD flag) @safe nothrow
			{ return !!(pfd.dwFlags & flag); }

			if(usablePixelFormat(pfd, accelerated))
			{
				enforceSys!SetPixelFormat(dcHandle, pixelFormatIndex, &pfd);
				break;
			}

			enforce(pixelFormatIndex != maxDisplayableFormatIndex,
				"Can't find any suitable pixel format.");
		}

		HGLRC glrc = _glrc = enforceSys!wglCreateContext(dcHandle);
		scope(failure) enforceSys!wglDeleteContext(glrc);

		enforceSys!wglMakeCurrent(dcHandle, glrc);
	}

	~this()
	{
		scope(exit) enforceSys!
			UnregisterClassW(cast(LPCWSTR) _classAtom, _processInstance);
		scope(exit) enforceSys!DestroyWindow(_handle);

		scope(exit) enforceSys!wglDeleteContext(_glrc);
		enforceSys!wglMakeCurrent(null, null);
	}

	@property pure nothrow
	{
		inout(HDC) dcHandle() inout
		{ return _dcHandle; }

		bool accelerated() const
		{ return _accelerated; }
	}
}


struct CreateContextExtensions
{
	FramebufferExtensions fb;

	bool has_ARB_create_context_profile = false;

	extern(Windows) nothrow
	{
		// WGL_ARB_create_context
		HGLRC function(HDC, HGLRC, const(int)*)
			createContextAttribsARB = null;
	}

	this(in bool useAccelerationIfAny)
	{
		this(TempContext(useAccelerationIfAny).dcHandle);
	}

	private this(HDC dcHandle) nothrow
	in { assert(dcHandle); }
	body
	{
		const extensions = getWGLExtensions(dcHandle);
		this(str => !!(str in extensions));
	}

	private this(scope bool delegate(in char[]) nothrow extensionSupported) nothrow
	{
		fb = FramebufferExtensions(extensionSupported);

		if(extensionSupported("WGL_ARB_create_context"))
		{
			tryLoadWGL!createContextAttribsARB("wglCreateContextAttribsARB");

			if(extensionSupported("WGL_ARB_create_context_profile"))
				has_ARB_create_context_profile = true;
		}
	}
}


/**
Creates an OpenGL context and makes it current.

Created context is suitable for using with any device context
sharing the same pixel format as $(D dcHandle), and created
on the same device.
*/
HGLRC createContext(HDC dcHandle, HGLRC shareContext, in ContextConfig config, in CreateContextExtensions ccExt)
in { config.validate(); }
body
{
	HGLRC context;
	const wglCreateContextAttribsARB = ccExt.createContextAttribsARB;

	if(wglCreateContextAttribsARB)
	{
		int[9] attributes =
		[
			0x2091 /* WGL_CONTEXT_MAJOR_VERSION_ARB */,
			config.version_.major,
			0x2092 /* WGL_CONTEXT_MINOR_VERSION_ARB */,
			config.version_.minor,
			// flags & profile:
			0, 0,
			0, 0,
			0
		];

		size_t i = 3;

		const bool forwardCompatible = config.compatibility == Compatibility.forward;
		if(forwardCompatible || config.debug_)
		{
			attributes[++i] = 0x2094 /* WGL_CONTEXT_FLAGS_ARB */;
			attributes[++i] =
				(forwardCompatible ? 2 /* WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB */ : 0) |
				(config.debug_ ? 1 /* WGL_CONTEXT_DEBUG_BIT_ARB */: 0);
		}

		if(config.version_.hasProfile)
		{
			if(!ccExt.has_ARB_create_context_profile)
				throw new SysException("OpenGL 3.2 and later versions are not supported by OpenGL " ~
					"implementation as `WGL_ARB_create_context_profile` isn't supported.");

			attributes[++i] = 0x9126 /* WGL_CONTEXT_PROFILE_MASK_ARB */;
			attributes[++i] = config.compatibility != Compatibility.backward ?
				1 /* WGL_CONTEXT_CORE_PROFILE_BIT_ARB */ :
				2 /* WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB */;
		}

		context = enforceSys!wglCreateContextAttribsARB(dcHandle, shareContext, attributes.ptr,
		[
			0x2095 /* WGL_ERROR_INVALID_VERSION_ARB */ : "Invalid version.",
			0x2096 /* WGL_ERROR_INVALID_PROFILE_ARB */ : "Invalid profile.",
		]);
	}
	else
	{
		if(config.compatibility == Compatibility.forward)
			throw new SysException("Forward compatible OpenGL context is not supported by OpenGL " ~
				"implementation as `WGL_ARB_create_context` isn't supported.");

		if(config.version_.hasProfile && config.compatibility == Compatibility.no)
			throw new SysException("OpenGL core context is not supported by OpenGL " ~
				"implementation as `WGL_ARB_create_context` isn't supported.");

		if(config.debug_)
			throw new SysException("Debug OpenGL context is not supported by OpenGL " ~
				"implementation as `WGL_ARB_create_context` isn't supported.");

		context = enforceSys!wglCreateContext(dcHandle);
	}

	scope(failure) enforceSys!wglDeleteContext(context);

	HGLRC prevContext = wglGetCurrentContext();
	HDC prevDC = prevContext ? wglGetCurrentDC() : null;
	enforceSys!wglMakeCurrent(dcHandle, context);
	scope(failure) enforceSys!wglMakeCurrent(prevDC, prevContext);

	if(wglCreateContextAttribsARB)
	{
		if(config.compatibility == Compatibility.backward &&
			config.version_ == Version(3, 1))
		{
			const currConfig = ContextConfig.current;

			if(currConfig.compatibility != Compatibility.backward)
				throw new SysException(format("Non backward-compatible OpenGL %s " ~
					"context was created with `wglCreateContextAttribsARB` when " ~
					"requesting 3.1 context. There is no way to explicitly request " ~
					"backward-compatible 3.1 context with this function.",
					currConfig.version_));
		}
	}
	else
	{
		const currVersion = ContextConfig.currentVersion;

		if(currVersion < config.version_)
			throw new SysException(format("OpenGL %s version is not supported by OpenGL " ~
				"implementation. `WGL_ARB_create_context_profile` isn't supported " ~
				"and `wglCreateContext` creates only %s version.",
				config.version_, currVersion));
	}

	if(shareContext)
		enforceSys!wglShareLists(shareContext, context);

	return context;
}

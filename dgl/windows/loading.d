﻿/**
WGL functions for function loading.

Copyright: Denis Shelomovskij 2014

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module dgl.windows.loading;


import core.sys.windows.windows;

import std.string: toStringz;
import std.traits: functionLinkage;

import dgl.utils;
import dgl.windows.sysapi;
import dgl.windows.exception;


T tryLoadWGL(T)(in string name)
if(functionLinkage!T == "Windows")
{
	// Some implementations may return from -1 to 3 on failure:
	const PROC proc = wglGetProcAddress(name.toStringz());
	if(cast(size_t) proc + 1 <= 4)
		return null;
	return cast(T) proc;
}

bool tryLoadWGL(alias func)(in string name = __traits(identifier, func))
{
	func = tryLoadWGL!(typeof(func))(name);
	return !!func;
}

bool tryLoadWGL(T)(ref T func, in string name)
{
	func = tryLoadWGL!T(name);
	return !!func;
}

T loadWGL(T)(in string name, in string file = __FILE__, in size_t line = __LINE__)
{
	if(T func = tryLoadWGL!T(name))
		return func;
	throw new SysException("'wglGetProcAddress' failed to load '" ~ name ~ "'.", file, line);
}

void loadWGL(alias func)(in string name = __traits(identifier, func), in string file = __FILE__, in size_t line = __LINE__)
{
	func = loadWGL!(typeof(func))(name, file, line);
}

void loadWGL(T)(ref T func, in string name, in string file = __FILE__, in size_t line = __LINE__)
{
	func = loadWGL!T(name, file, line);
}

bool[const(char)[]] getWGLExtensions(HDC dcHandle) nothrow
{
	bool[const(char)[]] extensions;

	// WGL_ARB_extensions_string
	extern(Windows) const(char)* function(in HDC) nothrow wglGetExtensionsStringARB;

	// WGL_EXT_extensions_string
	extern(Windows) const(char)* function() nothrow wglGetExtensionsStringEXT;

	const(char)* extensionsCStringARB = null;
	if(tryLoadWGL!wglGetExtensionsStringARB())
	{
		extensionsCStringARB = wglGetExtensionsStringARB(dcHandle);
		foreach(const ext; extensionsCStringARB.extensionsRange())
			extensions[ext] = true;
	}

	if(tryLoadWGL!wglGetExtensionsStringEXT())
	{
		const(char)* extensionsCStringEXT = wglGetExtensionsStringEXT();
		if(extensionsCStringEXT != extensionsCStringARB)
			foreach(const ext; extensionsCStringEXT.extensionsRange())
				extensions[ext] = true;
	}

	extensions.rehash();

	return extensions;
}

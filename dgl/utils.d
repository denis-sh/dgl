﻿/**
OpenGL helper functions.

Copyright: Denis Shelomovskij 2014

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module dgl.utils;


import std.exception: assumeWontThrow;
import std.string: format;

import dgl.glapi;


string glErrorString(in GLenum error) @safe pure nothrow
in { assert(error != GL_NO_ERROR); }
body
{
	const str =
	{
		switch(error)
		{
			case GL_INVALID_ENUM       : return "invalid enum";
			case GL_INVALID_VALUE      : return "invalid value";
			case GL_INVALID_OPERATION  : return "invalid operation";
			case GL_STACK_OVERFLOW     : return "stack overflow";
			case GL_STACK_UNDERFLOW    : return "stack underflow";
			case GL_OUT_OF_MEMORY      : return "out of memory";

			// Extension errors:
			case 0x0506 /* GL_INVALID_FRAMEBUFFER_OPERATION{,_EXT,_OES} */:
				return "invalid framebuffer operation";
			case 0x8031 /* GL_TABLE_TOO_LARGE{,_EXT} */:
				return "table too large";
			case 0x8065 /* GL_TEXTURE_TOO_LARGE_EXT */:
				return "texture too large";
			default:
				return "unknown error";
		}
	} ();

	return format("%s (0x%.4X)", str, error).assumeWontThrow();
}

unittest
{
	assert(glErrorString(GL_INVALID_ENUM) == "invalid enum (0x0500)");
	assert(glErrorString(0x12) == "unknown error (0x0012)");
}


auto extensionsRange(const(char)* extensionsCString) pure nothrow
{
	static struct Result
	{
	pure nothrow:

		private
		{
			const(char)[] curr;
			const(char)* rest;
		}

		@property
		{
			auto save() inout
			{ return this; }

			bool empty() const
			{ return !curr; }

			const(char)[] front() const
			in { assert(!empty, "Trying to call `front` on an empty extensions range."); }
			body { return curr; }
		}

		void popFront()
		in { assert(!empty, "Trying to call `popFront` on an empty extensions range."); }
		body
		{
			while(*rest == ' ')
				++rest;
			if(*rest == '\0')
			{
				curr = null;
				return;
			}
			const char* start = rest;
			do
				++rest;
			while(*rest != ' ' && *rest != '\0');
			curr = start[0 .. rest - start];
		}
	}

	if(!extensionsCString)
		return Result(null);

	auto res = Result("", extensionsCString);
	res.popFront();
	return res;
}

@trusted pure nothrow unittest
{
	import std.algorithm;

	alias r = extensionsRange;
	assert(r("").empty);
	assert(r("  ").empty);
	assert(r("a").equal(["a"]));
	assert(r("a bc").equal(["a", "bc"]));
	assert(r("  ab  cd  ").equal(["ab", "cd"]));
}


private ulong _errors = 0;

/// OpenGL error helper functions
@property ulong errors() @safe nothrow
{ return _errors; }

/// ditto
void clearErrors() @safe nothrow
{ _errors = 0; }

/// ditto
void printGLErrors(string state)
{
	import std.stdio;

	GLenum glError;
	while((glError = glGetError()) != GL_NO_ERROR)
	{
		stderr.writefln("GL error #%s at %s: %s", ++_errors, state, glErrorString(glError));
	}
}

﻿/**
OpenGL API declarations.

Copyright: Denis Shelomovskij 2014

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module dgl.glapi;


pragma(lib, "OpenGL32");

extern(System) nothrow extern:

alias GLenum = uint;
alias GLint = int;
alias GLubyte = ubyte;
alias GLuint = uint;

enum : int
{
	// 1.1

	GL_NO_ERROR = 0,
	GL_INVALID_ENUM = 0x0500,
	GL_INVALID_VALUE = 0x0501,
	GL_INVALID_OPERATION = 0x0502,
	GL_OUT_OF_MEMORY = 0x0505,

	GL_STACK_OVERFLOW = 0x0503,
	GL_STACK_UNDERFLOW = 0x0504,

	GL_VERSION = 0x1F02,
	GL_EXTENSIONS = 0x1F03,


	// 3.0

	GL_NUM_EXTENSIONS = 0x821D,
	GL_CONTEXT_FLAGS = 0x821E,
	GL_CONTEXT_FLAG_FORWARD_COMPATIBLE_BIT = 0x00000001,


	// 3.2

	GL_CONTEXT_COMPATIBILITY_PROFILE_BIT = 0x00000002,

	GL_CONTEXT_PROFILE_MASK = 0x9126,
}

// 1.0

void glFinish();
GLenum glGetError();
void glGetIntegerv(GLenum pname, GLint* data);
const(GLubyte)* glGetString(GLenum name);

// 3.0

alias GetStringi = const(GLubyte)* function(GLenum name, GLuint index);
